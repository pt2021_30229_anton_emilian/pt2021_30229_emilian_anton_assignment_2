import controller.Controller;
import View.View;

public class MainClass {
    public static void main(String argv[])
    {
        View view = new View();
        Controller controller = new Controller(view);
    }
}
