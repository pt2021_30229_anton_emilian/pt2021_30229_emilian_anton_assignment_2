package Model;

import View.ViewAction;
import utility.Log;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Store extends Thread
{
    private AtomicInteger time = new AtomicInteger();
    private int simulationTime ;
    private int nbOfQueues;
    private  float avgServiceTime = 0 ;
    private  float processedClients = 0 ;
    private List<Client> clients ;
    private List<Queue>  queues ;
    private int nbClients;
    private ViewAction viewAction ;

    public Store(int nbClients  , int nbQueues , int simulationTime , int timeArrivalMin , int timeArrivalMax, int timeServiceMin, int timeServiceMax) {
        this.simulationTime = simulationTime ;
        this.nbClients = nbClients;
        this.nbOfQueues = nbQueues ;
        this.queues = new ArrayList<>();
        generateRandomClients(timeArrivalMin , timeArrivalMax , timeServiceMin , timeServiceMax);
    }

    public int getNbOfQueues() {
        return nbOfQueues;
    }
    public void createQueues(){
        for (int i = 0 ; i < nbOfQueues ; i++){
            Queue q = new Queue(time  , i );
            q.setName("Model.Queue"+i);
            queues.add(q);
        }
    }
    public void startQueues(){
        createQueues();
        for (int i = 0 ; i < nbOfQueues ; i++){
            queues.get(i).setDaemon(true);
            queues.get(i).start();
        }
    }
    public void stopQueues(){
        for (int i = 0 ; i < nbOfQueues ; i++){
            queues.get(i).setValid(false);
        }

        try {
            Log.getInstance().writeToFile("\nStatistics for each queue:" );
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        for (int i = 0 ; i < nbOfQueues ; i++){
            int peakHour = queues.get(i).getPeakHour();
            int maxClients = queues.get(i).getMaxClients();
            float avgWaitTime = queues.get(i).getAvgWaitingTime() / queues.get(i).getClientsProcessed();
            try {
                Log.getInstance().writeToFile("The peak hour was " + peakHour + " with " +maxClients +" clients for " + queues.get(i).getName() );
                Log.getInstance().writeToFile("The average waiting time was " + avgWaitTime +" for " + queues.get(i).getName()+"\n" );
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

    }
    public int getMinWaitingQueue() {
        int index = 0 ;
        int min  = queues.get(0).getWaitingTime();
        for (int i = 1 ; i < nbOfQueues ; i++){
            if (min > queues.get(i).getWaitingTime())
            {
                min = queues.get(i).getWaitingTime();
                index = i ;
            }
        }
        return  index;
    }

    public void addClientStore(Client c) {
        int index = getMinWaitingQueue();
        queues.get(index).addClient(c);
    }

    public void generateRandomClients(int timeArrivalMin , int timeArrivalMax, int timeServiceMin, int timeServiceMax) {
        this.clients = new ArrayList<Client>();
        for (int i = 0; i < nbClients; i++) {
            int arrivalTime = (int) (Math.random() * (timeArrivalMax - timeArrivalMin + 1) + timeArrivalMin);
            int serviceTime = (int) (Math.random() * (timeServiceMax - timeServiceMin + 1) + timeServiceMin);
            Client newClient = new Client(i + 1, arrivalTime, serviceTime);
            clients.add(newClient);
        }
        Collections.sort(clients);
        for (Client c : clients) {
            try {
                Log.getInstance().writeToFile(c.toString());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        try {
            Log.getInstance().writeToFile("\n");
        } catch (FileNotFoundException e) {
            e.printStackTrace();

        }
    }
    public float getAvarageTimeService(){
        return  avgServiceTime/ processedClients;
    }
    @Override
    public synchronized void run()
    {
        startQueues();
        int currentTime=time.get();
        // bag in cozi
        while(currentTime <= simulationTime) {
            for ( int i = 0 ; i < clients.size() ; i++) {
                if ((clients.get(i).getArrivalTime() == currentTime) && (clients.get(i).getServiceTime()+currentTime <= simulationTime )) //
                {
                    //System.out.println("New Model.Client Added");
                    addClientStore(clients.get(i));// il bagam in store
                    avgServiceTime+=clients.get(i).getServiceTime();
                    processedClients++;
                }
            }
            viewAction = ViewAction.getInstance();
            String text = "" ;
            if (clients.size()!=0)
                for ( int i = 0 ; i < clients.size() ; i++) {
                    text = text + clients.get(i).toString2()+" ";
                }
            else text = "empty";

            viewAction.sendClientList(text);
            try {
                wait(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            int finalTime = time.get();
            /// remove the clients that have arrived
            clients.removeIf(c->c.getArrivalTime() == finalTime);

            text = "" ;
            if (clients.size()!=0)
                for ( int i = 0 ; i < clients.size() ; i++) {
                    text = text + clients.get(i).toString2()+" ";
                }
            else text = "empty";
            viewAction.sendClientList(text);

            time.incrementAndGet();
            currentTime++;

            System.out.println();
            if (time.get() == simulationTime+1) {
                stopQueues();
                System.out.println("Average Service time is :" + this.getAvarageTimeService());
                try {
                    Log.getInstance().writeToFile("Average service time is :" + this.getAvarageTimeService());
                    Log.getInstance().closeFile();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
