package Model;

import View.ViewAction;
import utility.Log;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Queue extends Thread
{
    private AtomicInteger time;
    private boolean valid = true;
    private int id ;
    private int waitingTime ;
    private List<Client> waitingClients = new ArrayList<>();
    private Client activeClient; /// pe el in proceseaza
    private Log log ;
    private int peakHour = 0 ;
    private int maxClients = 0 ;
    private int avgWaitingTime = 0 ;
    private int clientsProcessed = 0 ;
    private ViewAction viewAction ;

    public Queue (AtomicInteger time , int id ) {
        viewAction = ViewAction.getInstance();
        waitingTime = 0 ;
        this.id = id ;
        activeClient = null ;
        this.time = time;
        try {
            log = Log.getInstance();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public int getPeakHour() {
        return peakHour;
    }

    public int getMaxClients() {
        return maxClients;
    }
    public int getAvgWaitingTime() {
        return avgWaitingTime;
    }
    public int getClientsProcessed() {
        return clientsProcessed;
    }

    public void addClient(Client c) {
        waitingClients.add(c);
        waitingTime += c.getServiceTime();
    }

    public int getQId() {
        return id;
    }

    public int getWaitingTime() {
        return waitingTime;
    }

    public synchronized void run()
    {
        while(valid)
        {
            if (activeClient == null) {
                if (!waitingClients.isEmpty()) {
                    activeClient = waitingClients.remove(0);
                    avgWaitingTime += (time.get() - activeClient.getArrivalTime()); // avg waiting time is the difference of current time and the client arrival time
                    clientsProcessed++;
                } else {
                    //System.out.println("Empty Model.Queue");
                }
            }
            if(activeClient != null) {
                System.out.println("Time: " + time.get() + " Th."+Thread.currentThread().getName()+" Active:" + activeClient.toString());
                log.writeToFile("Time: " + time.get() + " Th."+Thread.currentThread().getName()+" Active:" + activeClient.toString());
                viewAction.sendText("T:" + time.get()+" "+activeClient.toString2() , getQId());
            }
            else{
                System.out.println("Time: " + time.get() +" Th."+Thread.currentThread().getName()+" Active client null");
                log.writeToFile("Time: " + time.get() +" Th."+Thread.currentThread().getName()+" Active client null");
                viewAction.sendText("T:"+time.get()+ ":null" , getQId());
            }
            if (waitingClients.size() > maxClients) { /// stats for peak hour and maximum people waiting at that queue
                maxClients = waitingClients.size() ;
                peakHour = time.get();
            }


            if(activeClient != null && !waitingClients.isEmpty()){
                String txt = "";
                System.out.print("Waiting list " + Thread.currentThread().getName() + ":");/// waiting list
                for (Client c : waitingClients ){
                    System.out.print( " "+c.toString());
                    txt = txt + c.toString2() + " " ;
                }
                viewAction.sendTextList(txt,getQId());
                System.out.println();
            }
            else
            {
                viewAction.sendTextList("empty",getQId());
            }


            try {
                wait(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (activeClient!=null) {
                if (waitingTime > 0){
                    waitingTime--; // pe queue
                    activeClient.setServiceTime(activeClient.getServiceTime() - 1);
                }
            }

            if (activeClient!=null) {
                if (activeClient.getServiceTime() == 0) {
                    activeClient = null;
                }
            }
        }
    }
}
