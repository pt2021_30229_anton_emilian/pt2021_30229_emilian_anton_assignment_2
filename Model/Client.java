package Model;

public class Client implements Comparable {
    private int id ;
    private int arrivalTime ;
    private int serviceTime ;

    public Client(int id , int arrTime , int servTime){
        this.id = id ;
        this. arrivalTime = arrTime ;
        this.serviceTime = servTime ;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(int arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public int getServiceTime() {
        return serviceTime;
    }

    public void setServiceTime(int serviceTime) {
        this.serviceTime = serviceTime;
    }

    @Override
    public String toString() {
        return "Model.Client:" +
                "id=" + id +
                " arrivalTime=" + arrivalTime +
                " serviceTime=" + serviceTime +
                ';';
    }
    public String toString2() {
        return "("+id +"," +arrivalTime + "," +serviceTime+")";
    }
    @Override
    public int compareTo(Object o) {
        Client c = (Client) o ;
        if (this.getArrivalTime() == c.getArrivalTime()){
            return 0 ;
        }
        else if (this.getArrivalTime()  > c.getArrivalTime()){
            return  1 ;
        }
        else return -1;
    }
}
