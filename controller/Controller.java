package controller;

import Model.Store;
import View.SecondView;
import View.View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller
{
    private View view;
    private Store model;
    public Controller(View view){
        this.view = view ;
       // this.model = model;
        view.startListener(new StartLst());
    }

    class StartLst implements ActionListener {
        public void actionPerformed(ActionEvent e){

            int nbClients =Integer.parseInt(view.getTextClients());
            int nbQueues = Integer.parseInt(view.getTextQueues());

            int simTime = Integer.parseInt(view.getTextSimTime());

            int minArrTime = Integer.parseInt(view.getTextArrMin());
            int maxArrTime = Integer.parseInt(view.getTextArrMax());
            int maxSerTime = Integer.parseInt(view.getTextSerMax());
            int minSerTime = Integer.parseInt( view.getTextSerMin());

            model = new Store(nbClients , nbQueues , simTime , minArrTime , maxArrTime , minSerTime , maxSerTime);

            SecondView secondView = new SecondView(model);

            model.start();

        }
    }

}
