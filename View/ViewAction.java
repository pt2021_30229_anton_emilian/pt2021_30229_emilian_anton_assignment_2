package View;

import View.SecondView;

public class ViewAction {
    private static ViewAction instance ;
    private static Object obj = new Object();
    private SecondView secondView;

    private ViewAction(SecondView secondView) {
        this.secondView = secondView ;
    }

    public static ViewAction getInstance() { // univ
        return instance;
    }

    public static ViewAction createInstance(SecondView secondView) {
        if (instance == null){
            synchronized (obj){
                if (instance == null){
                    instance = new ViewAction(secondView);
                }
            }
        }
        return instance;
    }
    public void sendText(String text, int index) {
        secondView.setItextField(text, index);
    }
    public void sendTextList(String text, int index) {
        secondView.setItextFieldList(text, index);
    }
    public void sendClientList(String text){
        secondView.setTextClientList(text);
    }

}