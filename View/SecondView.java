package View;

import Model.Store;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class SecondView extends JFrame {
    private Store model ;
    private List<JTextField> tf = new ArrayList<>();
    private List<JTextField> tf2 = new ArrayList<>();
    private List<JPanel> jp = new ArrayList<>();
    private JPanel allPanels = new JPanel();
    private JPanel clientsList;
    private JTextField clientsListTxt = new JTextField();
    private ViewAction viewAction ;

    public SecondView(Store model)
    {
        //@@ creating the instance of viewAction
        viewAction = ViewAction.createInstance(this);

        this.setTitle("Model.Queue Simulator");
        this.model = model ;
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(500, 500);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        this.setLayout(null);
        this.getContentPane().setBackground(new Color(245, 255, 255, 111));

        clientsList= new JPanel();
        clientsList.setBounds(0 , 0 , 450 , 50);
        clientsList.add(new JLabel("Clients:"));

        clientsListTxt.setPreferredSize(new Dimension(350,40));
        clientsListTxt.setEditable(false);
        clientsList.add(clientsListTxt);
        allPanels.add(clientsList);

        for (int i = 0; i < model.getNbOfQueues(); i++)
        {
            JPanel newJpanel = new JPanel();
            newJpanel.setBounds(0, 50 * (i+1), 450, 50);

            JTextField newTf = new JTextField();
            newTf.setEditable(false);
            newTf.setPreferredSize(new Dimension(100,40));

            JTextField newTf2 = new JTextField();
            newTf2.setEditable(false);
            newTf2.setPreferredSize(new Dimension(200,40));

            tf.add(newTf); // add to Text list
            tf2.add(newTf2);

            JLabel newJlab = new JLabel("Queue" + i + ": ");
            newJpanel.add(newJlab);
            newJpanel.add(newTf);
            newJpanel.add(newTf2);

            jp.add(newJpanel); // add to Panel list
            allPanels.add(jp.get(i));
        }
        allPanels.setLayout(null);

        this.setContentPane(allPanels);
    }
    public void setItextField(String text , int index){
        tf.get(index).setText(text);
    }
    public void setItextFieldList(String text , int index){
        tf2.get(index).setText(text);
    }

    public void setTextClientList(String text){
        clientsListTxt.setText(text);
    }

}
