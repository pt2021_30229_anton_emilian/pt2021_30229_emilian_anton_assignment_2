package View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class View extends JFrame {

    private JTextField nbOfClients = new JTextField();
    private JTextField nbOfQueues  = new JTextField();
    private JTextField simulationTime  = new JTextField();
    private JTextField timeArrivalMax  = new JTextField();
    private JTextField timeArrivalMin  = new JTextField();
    private JTextField timeServiceMax  = new JTextField();
    private JTextField timeServiceMin  = new JTextField();
    private JButton startBtn = new JButton("Start");
    private JPanel simulationPanel = new JPanel();
    public View()
    {

        //simulationPanel.setBackground(new Color(32, 203, 87, 111));
       // simulationPanel.setBounds(200 , 5 , 150 , 450 );

        nbOfClients.setPreferredSize(new Dimension(50 , 30));
        nbOfQueues.setPreferredSize(new Dimension(50 , 30));
        simulationTime.setPreferredSize(new Dimension(50 , 30));
        timeServiceMax.setPreferredSize(new Dimension(50 , 30));
        timeServiceMin.setPreferredSize(new Dimension(50 , 30));
        timeArrivalMin.setPreferredSize(new Dimension(50 , 30));
        timeArrivalMax.setPreferredSize(new Dimension(50 , 30));

        JPanel panel1 = new JPanel();
        panel1.setBounds(10 , 0 , 150 , 50 );
        panel1.add( new JLabel("Number of clients:")) ;
        panel1.add( nbOfClients );

        JPanel panel2 = new JPanel();
        panel2.setBounds(10 , 50 , 150 , 50 );
        panel2.add( new JLabel("Number of queues:")) ;
        panel2.add( nbOfQueues );

        JPanel panel3 = new JPanel();
        panel3.setBounds(10 , 100 , 150 , 50 );
        panel3.add( new JLabel("Simulation Time:")) ;
        panel3.add( simulationTime );

        JPanel panel4 = new JPanel();
        panel4.setBounds(10 , 150 , 150 , 50 );
        panel4.add( new JLabel("Minimum Arrival Time:")) ;
        panel4.add( timeArrivalMin );

        JPanel panel5 = new JPanel();
        panel5.setBounds(10 , 200 , 150 , 50 );
        panel5.add( new JLabel("Maximum Arrival Time:")) ;
        panel5.add( timeArrivalMax );

        JPanel panel6 = new JPanel();
        panel6.setBounds(10 , 250 , 150 , 50 );
        panel6.add( new JLabel("Minimum Service Time:")) ;
        panel6.add( timeServiceMin );


        JPanel panel7 = new JPanel();
        panel7.setBounds(10 , 300 , 150 , 50 );
        panel7.add( new JLabel("Maximum Service Time:")) ;
        panel7.add( timeServiceMax );

        JPanel panelOp = new JPanel();
        panelOp.setBounds(10 , 375 , 150 , 150 );
        panelOp.add(startBtn);

        this.setTitle("Model.Queue Simulator");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(200,500);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        this.setLayout(null);
        this.getContentPane().setBackground(new Color(245, 255, 255, 111));

        JPanel allPanels = new JPanel();
        allPanels.add(panel1);
        allPanels.add(panel2);
        allPanels.add(panel3);
        allPanels.add(panel4);
        allPanels.add(panel5);
        allPanels.add(panel6);
        allPanels.add(panel7);
        allPanels.add(panelOp);
        allPanels.setLayout(null);

        this.setContentPane(allPanels);
    }
    public String getTextClients() {
        return nbOfClients.getText();
    }
    public String getTextQueues() {
        return nbOfQueues.getText();
    }
    public String getTextSimTime() {
        return simulationTime.getText();
    }
    public String getTextArrMin() {
        return timeArrivalMin.getText();
    }
    public String getTextArrMax() {
        return timeArrivalMax.getText();
    }
    public String getTextSerMin() {
        return timeServiceMin.getText();
    }
    public String getTextSerMax() {
        return timeServiceMax.getText();
    }
    public void startListener(ActionListener startLst){
        startBtn.addActionListener(startLst);
    }

}
